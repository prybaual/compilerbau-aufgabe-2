%option noyywrap
%{
    #include "urlscanner.h"
%}
WHITESPACE  [ \t\n\r]

%s URL
%s TAG
%s VTAG
%s TAG2
%s VTAG2


%%
"\n"            {yylineno++;}
"<a"    {BEGIN (TAG);}

<TAG>"href"     {BEGIN (VTAG);}
<TAG>.          {}

<VTAG>\"        {BEGIN (URL);}
<VTAG>.         {}

<URL>[^\"\n]*   {yylval = yytext; 
                return TOKEN_URL;}
<URL>\"         {BEGIN (VTAG2);} 

<VTAG2>">"      {BEGIN (TAG2);}
<VTAG2>.        {}   


<TAG2>[^\<\n]*   {yylval = yytext;
                return TOKEN_TEXT;
                BEGIN (VTAG);}
<TAG2>"</a"     {BEGIN (INITIAL);}

<<EOF>>         {return MYEOF;}

.               {}

%%
