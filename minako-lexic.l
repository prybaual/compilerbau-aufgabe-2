%option noyywrap
%{
#include "minako.h"
%}
WHITESPACE  [ \t\r]
DIGIT       [0-9]
INTEGER     {DIGIT}+        
FLOAT       {INTEGER}"."{INTEGER}|"."{INTEGER}
LETTER      [a-zA-Z]

FLOAT2      {FLOAT}([eE]([-+])?{INTEGER})?|{INTEGER}[eE]([-+])?{INTEGER}    
ID          ({LETTER})+({DIGIT}|{LETTER})*
STRING      "\""[^\n\"]*"\""
/*Zustände*/
%x Comment1
%x Comment2
%x String

%%
{WHITESPACE}+   {}
"\n"            {yylineno++;}

"/*"            {BEGIN (Comment1);}
"//"            {BEGIN (Comment2);}
\"              {BEGIN (String);}

"&&"            {return AND;}
"||"            {return OR;}
"=="            {return EQ;}
"!="            {return NEQ;}
"<="            {return LEQ;}
">="            {return GEQ;}
"<"             {return LSS;}
">"             {return GRT;}
"bool"          {return KW_BOOLEAN;}
"do"            {return KW_DO;}
"else"          {return KW_ELSE;}
"float"         {return KW_FLOAT;}
"for"           {return KW_FOR;}
"if"            {return KW_IF;}
"int"           {return KW_INT;}
"printf"        {return KW_PRINTF;}
"return"        {return KW_RETURN;}
"void"          {return KW_VOID;}
"while"         {return KW_WHILE;}

{INTEGER}       {yylval.intValue = atoi(yytext);
                return CONST_INT;}
{FLOAT2}        {yylval.floatValue = atof(yytext);
                return CONST_FLOAT;}
"true"          {yylval.intValue = 1;
                return CONST_BOOLEAN;}
"false"         {yylval.intValue = 0;
                return CONST_BOOLEAN;}
{ID}            {yylval.string = yytext;
                return ID;}

"+"             |
"-"             |
"*"             |
"/"             |
"="             |
","             |
";"             |
"("             |
")"             |
"{"             |
"}"             return (int)yytext[0];

<Comment1>.     {}
<Comment1>\n    {yylineno++;}
<Comment1>"*/"  {BEGIN (INITIAL);}

<Comment2>.     {}
<Comment2>\n    {yylineno++;
                BEGIN (INITIAL);}

<String>[^\"]*   {yylval.string = yytext;
                return CONST_STRING;}
<String>\"      {BEGIN (INITIAL);}


<<EOF>>         {return EOF;}

.               {printf("unknown: %c\n", *yytext);
                exit(-1);}

%%